import React from 'react';
import style from "./main.css";
class Notes extends React.Component {
    constructor() {
        super()
        this.state = {
            textnote: "",
            typetext: true,
            ArrayNote: [],
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleChangenote = this.handleChangenote.bind(this);
        this.changeText = this.changeText.bind(this);
    }
    changeText = () => {
        this.setState(prevstate => {
            return ({
                typetext: !prevstate.typetext
            })
        });
        console.log(this.state)
    };
    handleChange(id, value) {
        this.setState({
            [id]: value
        });
        console.log(id);
        console.log(value);
    }
    handleChangenote(event) {
        const { value } = event.target
        this.setState({ textnote: value })
    }
    NoteAdd = (event) => {
        event.preventDefault()
        this.setState(prevstate => {
            const newNote = {
                content: prevstate.textnote,
                typetext: prevstate.typetext
            }
            return (
                {
                    ArrayNote: [...prevstate.ArrayNote, newNote],
                    textnote: ""
                }
            )
        });
    }
    render() {
        console.log(this.state);
        return (
            <div className="container-fluid">
                <p className="text-center">NOTES</p>
                <form className="row ">
                    <div className="col-md-11">
                        <textarea type="text"
                            name="textnote"
                            placeholder="Please enter your note here....."
                            className="noteBox"
                            id="input"
                            value={this.state.textnote}
                            onChange={this.handleChangenote} />
                    </div>
                    <div className="col-md-1">
                        <div onClick={this.changeText}>
                            {this.state.typetext ? <p className="normal">Normal</p> : <p className="agent">Urgent</p>}
                        </div>
                        <button type="button" className="btn btn-success btn-sm" onClick={this.NoteAdd} style={{ display: this.state.textnote.length > 0 ? "block" : "none" }} name="button">Save</button>
                    </div>
                </form>
                <div className="row">
                    {this.state.ArrayNote.map(item => {
                        return (
                            <div className="col-md-3 col-sm-4 col-2 border" >
                                <div className="row">
                                    <p className="col-md-9" >{item.content} </p>
                                    <p className="col-md-3">
                                        {item.typetext ? <p className="col-md-3 normal">normal</p> : <p className="col-md-3 agent">agent</p>}
                                    </p>
                                </div>
                            </div>
                        )
                    })}
                </div>
            </div>
        )
    }
}
export default Notes