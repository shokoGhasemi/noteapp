import React from 'react';
import ReactDOM from 'react-dom';
import "bootstrap/dist/css/bootstrap.min.css"
import Notes from './Notes'

ReactDOM.render(<Notes />, document.getElementById('root'));